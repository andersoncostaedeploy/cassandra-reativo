package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PlaylistItem {
    private Integer id;
    @JsonProperty("title")
    private String title = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("image")
    private String image = null;
    @JsonProperty("link")
    private PlaylistLink link = new PlaylistLink();

    public PlaylistItem title(String title) {
        this.title = title;
        return this;
    }

    public PlaylistItem descript(String description) {
        this.description = description;
        return this;
    }

    public PlaylistItem image(String image) {
        this.image = image;
        return this;
    }

    public PlaylistItem link(PlaylistLink link) {
        this.link = link;
        return this;
    }

}
