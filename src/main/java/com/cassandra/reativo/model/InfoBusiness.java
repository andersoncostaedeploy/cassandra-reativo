package com.cassandra.reativo.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class InfoBusiness extends ArrayList<InfoBusinessItem> implements Serializable {

    private static final long serialVersionUID = -305726463442998985L;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class InfoBusiness {\n");
        sb.append("}");
        return sb.toString();
    }

}
