package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@AllArgsConstructor
public class PlaylistSection implements Serializable {

    private static final long serialVersionUID = -305726463442998985L;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("descript")
    private String descript = null;

    @JsonProperty("items")
    private List<PlaylistItem> items = new ArrayList<PlaylistItem>();

    public PlaylistSection title(String title) {
        this.title = title;
        return this;
    }

    public PlaylistSection descript(String descript) {
        this.descript = descript;
        return this;
    }

    public PlaylistSection items(List<PlaylistItem> items) {
        this.items = items;
        return this;
    }

    public PlaylistSection addItemsItem(PlaylistItem itemsItem) {
        this.items.add(itemsItem);
        return this;
    }

}
