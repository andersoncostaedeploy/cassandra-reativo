package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Explore  implements Serializable {

    private static final long serialVersionUID = -305726463442998985L;

    @JsonProperty("title")
    @Id
    private String title = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("cards")
    @Valid
    private List<ExploreCards> cards = null;

    public Explore title(String title) {
        this.title = title;
        return this;
    }

    public Explore description(String description) {
        this.description = description;
        return this;
    }

    public Explore cards(List<ExploreCards> cards) {
        this.cards = cards;
        return this;
    }

    public Explore addCardsItem(ExploreCards cardsItem) {
        if (this.cards == null) {
            this.cards = new ArrayList<ExploreCards>();
        }
        this.cards.add(cardsItem);
        return this;
    }

}
