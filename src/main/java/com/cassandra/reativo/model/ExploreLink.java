package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode

public class ExploreLink {

    @JsonProperty("url")
    private String url = null;

    @JsonProperty("label")
    private String label = null;

    public ExploreLink url(String url) {
        this.url = url;
        return this;
    }

    public ExploreLink label(String label) {
        this.label = label;
        return this;
    }

}
