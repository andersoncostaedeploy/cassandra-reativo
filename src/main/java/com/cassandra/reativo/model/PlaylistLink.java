package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PlaylistLink implements Serializable {

    private static final long serialVersionUID = Long.BYTES;

    @JsonProperty("label")
    private String label = null;

    @JsonProperty("path")
    private String path = null;


}
