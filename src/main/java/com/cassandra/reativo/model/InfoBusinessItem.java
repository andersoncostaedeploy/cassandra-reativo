package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class InfoBusinessItem implements Serializable {

    private static final long serialVersionUID = -305726463442998985L;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("icon")
    private String icon = null;

    public InfoBusinessItem title(String title) {
        this.title = title;
        return this;
    }

    public InfoBusinessItem description(String description) {
        this.description = description;
        return this;
    }

    public InfoBusinessItem icon(String icon) {
        this.icon = icon;
        return this;
    }
}
