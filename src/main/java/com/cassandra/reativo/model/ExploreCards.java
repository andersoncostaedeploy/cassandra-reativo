package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ExploreCards {

    @JsonProperty("image")
    private String image = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("subtitle")
    private String subtitle = null;

    @JsonProperty("tags")
    @Valid
    private List<ExploreTag> tags = null;

    @JsonProperty("links")
    @Valid
    private List<ExploreLink> links = null;

    public ExploreCards image(String image) {
        this.image = image;
        return this;
    }

    public ExploreCards title(String title) {
        this.title = title;
        return this;
    }

    public ExploreCards subtitle(String subtitle) {
        this.subtitle = subtitle;
        return this;
    }

    public ExploreCards tags(List<ExploreTag> tags) {
        this.tags = tags;
        return this;
    }

    public ExploreCards addTagsItem(ExploreTag tagsItem) {
        if (this.tags == null) {
            this.tags = new ArrayList<ExploreTag>();
        }
        this.tags.add(tagsItem);
        return this;
    }

    public ExploreCards links(List<ExploreLink> links) {
        this.links = links;
        return this;
    }

    public ExploreCards addLinksItem(ExploreLink linksItem) {
        if (this.links == null) {
            this.links = new ArrayList<ExploreLink>();
        }
        this.links.add(linksItem);
        return this;
    }

}
