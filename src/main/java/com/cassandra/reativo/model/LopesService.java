package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class LopesService {

    @JsonProperty("icon")
    private String icon = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("path")
    private String path = null;

    public LopesService icon(String icon) {
        this.icon = icon;
        return this;
    }

    public LopesService title(String title) {
        this.title = title;
        return this;
    }

    public LopesService description(String description) {
        this.description = description;
        return this;
    }

    public LopesService path(String path) {
        this.path = path;
        return this;
    }

}
