package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class LopesServices {

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("items")
    @Valid
    private List<LopesService> items = null;

    public LopesServices title(String title) {
        this.title = title;
        return this;
    }

    public LopesServices description(String description) {
        this.description = description;
        return this;
    }

    public LopesServices items(List<LopesService> items) {
        this.items = items;
        return this;
    }

    public LopesServices addItemsItem(LopesService itemsItem) {
        if (this.items == null) {
            this.items = new ArrayList<LopesService>();
        }
        this.items.add(itemsItem);
        return this;
    }
 
}
