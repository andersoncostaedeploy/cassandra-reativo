package com.cassandra.reativo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode

public class ExploreTag {

    @JsonProperty("icon")
    private String icon = null;

    @JsonProperty("label")
    private String label = null;

    @JsonProperty("tag")
    private String tag = null;

    public ExploreTag icon(String icon) {
        this.icon = icon;
        return this;
    }

    public ExploreTag label(String label) {
        this.label = label;
        return this;
    }

    public ExploreTag tag(String tag) {
        this.tag = tag;
        return this;
    }
}
