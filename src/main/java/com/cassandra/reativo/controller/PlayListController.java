package com.cassandra.reativo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("Home")
public class PlayListController implements Serializable {

        private static final long serialVersionUID = -305726463442998985L;
        @Autowired
        private PlaylistService plalistService;

        @GetMapping(value = "/playlist", produces = MediaType.APPLICATION_JSON)
        public ResponseEntity<?> getPlaylist() {
            log.info("playlist");

            return new ResponseEntity<>(playlistService.getPlaylist(), HttpStatus.OK);
        }
}
