package com.cassandra.reativo.repository;

import com.cassandra.reativo.model.PlaylistItem;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepository extends CassandraRepository<PlaylistItem, String> {

    @Query("select * from book where title = ?0 and publisher=?1")
    Iterable<PlaylistItem> findByTitleAndPublisher(String title, String publisher);

}
