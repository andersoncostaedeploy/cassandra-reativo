package com.cassandra.reativo.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicosRepository extends CassandraRepository {

    @Query("select * from book where title = ?0 and publisher=?1")
    Iterable<ServicosRepository> findByTitleAndPublisher(String title, String publisher);

}
