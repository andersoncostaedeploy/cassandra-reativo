package com.cassandra.reativo.repository;

import com.cassandra.reativo.model.Explore;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface ExploreRepository extends CassandraRepository<Explore, Integer> {

    @AllowFiltering
    <ExploreLps>
    ExploreLps findByAgeGreaterThan(int age);
}
