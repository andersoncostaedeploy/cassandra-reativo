package com.cassandra.reativo.repository;

import com.cassandra.reativo.model.InfoBusiness;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InforRepository extends CassandraRepository<InfoBusiness, String> {

    @Query("select * from book where title = ?0 and publisher=?1")
    Iterable<InforRepository> findByTitleAndPublisher(String title, String publisher);

}
