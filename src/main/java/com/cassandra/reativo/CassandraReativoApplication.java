package com.cassandra.reativo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraReativoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CassandraReativoApplication.class, args);
    }
}
