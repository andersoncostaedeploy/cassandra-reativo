package com.cassandra.reativo.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lopes.portal.home.model.Home;
import com.lopes.portal.home.model.InfoBusiness;
import com.lopes.portal.home.redis.repo.JedisCache;
import com.lopes.portal.home.services.HomeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class InforServiceImpl implements HomeService {

    @Autowired
    private JedisCache cacheRepository;

    private ObjectMapper mapper;

    @Override
    public InfoBusiness getInfoBusiness() {
        this.mapper = new ObjectMapper();
        InfoBusiness infobusiness;
        try {
            String str = this.cacheRepository.get("info", "default_ptbr");
            log.info("info", str);
            infobusiness = this.mapper.readValue(str, InfoBusiness.class);
        } catch (Exception e) {
            log.error(e);
            infobusiness = null;
        }
        return infobusiness;
    }

    @Override
    public Home getHome() {
        Home home = new Home();
        home.setBusinessInformation(getInfoBusiness());

        return home;
    }

}
