package com.cassandra.reativo.service;


import com.cassandra.reativo.model.InfoBusiness;

public interface PlaylistService {

    InfoBusiness getInfoBusinessItems();

    Home getPlaylist();

}
