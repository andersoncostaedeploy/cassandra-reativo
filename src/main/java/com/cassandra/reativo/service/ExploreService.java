package com.cassandra.reativo.service;

import com.cassandra.reativo.repository.ExploreRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class ExploreService {

    @Autowired
    ExploreRepository exploreRepository;

    public void initializeExplore(List<Employee> employees) {
        Flux<Employee> savedEmployees = exploreRepository.saveAll(employees);
        savedEmployees.subscribe();
    }

    public Flux<Employee> getAllEmployees() {
        Flux<Employee> employees =  exploreRepository.findAll();
        return employees;
    }

    public Flux<Employee> getEmployeesFilterByAge(int age) {
        return exploreRepository.findByAgeGreaterThan(age);
    }

    public Mono<Employee> getEmployeeById(int id) {
        return exploreRepository.findById(id);
    }
}
