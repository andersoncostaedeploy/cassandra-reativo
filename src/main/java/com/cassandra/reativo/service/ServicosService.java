package com.cassandra.reativo.service;


import com.cassandra.reativo.model.InfoBusiness;

public interface ServicosService {

    InfoBusiness getInfoBusinessItems();

    Home getPlaylist();

}
