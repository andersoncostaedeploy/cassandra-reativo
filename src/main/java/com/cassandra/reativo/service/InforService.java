package com.cassandra.reativo.service;


import com.cassandra.reativo.model.InfoBusiness;

public interface InforService {

    InfoBusiness getInfoBusinessItems();

    Home getPlaylist();

}
